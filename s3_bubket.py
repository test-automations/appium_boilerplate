import boto3
from  config import aws_access_key_id ,aws_secret_access_key


client = boto3.client('s3',aws_access_key_id = aws_access_key_id,aws_secret_access_key=aws_secret_access_key,region_name='ap-south-1')


def createBucket(bucket_name,region='ap-south-1'):  #createBucket("devicefarmpackages")
    response = client.create_bucket(
        ACL='private',
        Bucket=bucket_name,
        CreateBucketConfiguration={
            'LocationConstraint': region,
        }
    )


def s3Upload(file_path,bucket_name,s3_folder=''):  # s3Upload('/home/shubham/Desktop/dwm_client_app_automation/app.zip','devicefarmpackages')
    with open(file_path,'rb') as f:
        data = f.read()
    response = client.put_object(
        ACL='private',
        Bucket=bucket_name,
        Body = data,
        Key = s3_folder+file_path.split("/")[len(file_path.split("/"))-1]
    )

def s3DeleteFile(bucket_name,key):
    response = client.delete_object(
        Bucket=bucket_name,
        Key=key
    )    

def s3FetchAllObjects(bucket_name):
    response = client.list_objects(
    Bucket=bucket_name
    )
    print(response)

def s3FetchFile(bucket_name,key):
    response = client.get_object(
    Bucket=bucket_name,
    Key=key
    )
    return response['Body'].read()

def s3FetchFileSave(bucket_name,key,save_as):
    response = client.get_object(
    Bucket=bucket_name,
    Key=key
    )
    with open(save_as,'wb') as f:
        f.write(response['Body'].read())

def s3UpdateFile(bucket_name,key,file_path,s3_folder):
    s3DeleteFile(bucket_name,key)
    s3Upload(file_path,bucket_name,s3_folder='')



