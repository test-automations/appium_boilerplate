from config import implicit_wait ,remote_host ,desired_capabilities
from appium import webdriver


driver = webdriver.Remote(remote_host, desired_capabilities )
driver.implicitly_wait(implicit_wait)