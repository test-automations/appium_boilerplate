desired_capabilities = {
"platformName": "Android",
"platformVersion": "11",
"deviceName": "Android Emulator",
"app":"/home/shubham/client_app_STAGE_updated_6jan.apk",
"appPackage": "com.anandrathi.pwmclientapp",
'newCommandTimeout':'3000',
"appActivity": "com.anandrathi.pwmclientapp.MainActivity",
"automationName": "UiAutomator2",
'ensureWebviewsHavePages':True
}

###########################################################################

SLACK_CHANNEL = '#slackexperiments'
SLACK_TOKEN = ""
###########################################################################

implicit_wait = 20
remote_host="http://0.0.0.0:4723/wd/hub"

###########################################################################

login_username=""
password=""

##########################################################################

aws_access_key_id = ''
aws_secret_access_key=''