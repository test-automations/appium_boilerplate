import sys
sys.path.append('appium_boilerplate/tests/HelperFunctions')
sys.path.append('appium_boilerplate/tests/pages')
sys.path.append('appium_boilerplate/tests/pages')
sys.path.append('appium_boilerplate') #for driver file

import os
import unittest
from cases import test_cases
from pages import login_page 
from driver import driver

from HelperFunctions import slack_file ,generateHtml

class MainTest(unittest.TestCase):
    report_data=[]

    @classmethod
    def tearDownClass(self):
        generateHtml(self.report_data)
        slack_file(fileName = os.getcwd()+'/APPIUM_pwm_client_app.html')


    def test_1(self):
        try:
            summary_button_text = login_page.Login.test_login(driver)
        except Exception as e:
            print(e)     


if __name__ == "__main__":
    unittest.main()
