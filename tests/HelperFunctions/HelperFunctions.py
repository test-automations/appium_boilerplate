import sys
sys.path.append('appium_boilerplate')

import time
import requests
from pprint import pprint
from config import  SLACK_CHANNEL ,SLACK_TOKEN

def num_pad(s,driver):
    time.sleep(.1)
    [driver.press_keycode(int(i)+7) for i in s]
    driver.execute_script('mobile: performEditorAction', {'action': 'done'})
    

def slack_file(channel=SLACK_CHANNEL , fileName,printResponse=False):
    url = "https://slack.com/api/files.upload"
    querystring = {"token":SLACK_TOKEN}
    payload = {
    "channels": channel}
    with open(fileName, 'rb') as f:
        file_upload = {
            "file":(fileName, f.read(), 'text/plain')
        }
    response = requests.post(url, data=payload,  params=querystring, files=file_upload)
    if printResponse:
        pprint(response.json())    


def slack_message(message,printResponse=False):
    print(message)
    data = {
        'token': SLACK_TOKEN,
        'channel': SLACK_CHANNEL,
        'as_user': True,
        'text': message
    }
    response = requests.post(url='https://slack.com/api/chat.postMessage',
                  data=data)
    if printResponse:
        pprint(response)


def generateHtml(data):
    header = """
    <!DOCTYPE html>
    <html>
    <head>
    <style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    </style>
    </head>
    <body>

    <h2>PWM CLIENT APP TEST REPORT</h2>

    <table>
        <tr>
        <th>TEST CASE</th>
        <th>STATUS</th>
        </tr>
    """

    footer = """
    </table>

    </body>
    </html>

    """  
    file=open("APPIUM_pwm_client_app.html",'w')
    body=""
    for i in data:
        body+=f"""  <tr>
    <td>{i[0]}</td>
    <td>{i[1]}</td>
  </tr>"""

    file.write( header + body + footer)
    file.close()



# slack_file(fileName="/home/shubham/Desktop/aws_device_farm_experiment/appium_dwm/test_bundle/testreport.csv")