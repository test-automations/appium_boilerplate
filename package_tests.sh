#!/bin/bash

## Remove cached files
find . -name '__pycache__' -type d -exec rm -r {} +
find . -name '.vscode' -type d -exec rm -r {} +
find . -name '*.pyc' -exec rm -f {} +
find . -name '*.pyo' -exec rm -f {} +
find . -name '*~' -exec rm -f {} +

## Build wheel archive
pip wheel --wheel-dir wheelhouse -r requirements.txt

## Zip tests/, wheelhouse/, and requirements.txt into test_bundle.zip
zip -r test_bundle.zip tests/ wheelhouse/ requirements.txt
